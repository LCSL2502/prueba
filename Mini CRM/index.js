//Primero obtenemos los elementos del documento por su ID para trabajar con ellos
let tituloV = document.getElementById("tituloVenta"),
    descripcionV = document.getElementById("descripcionVenta"),
    facturacionP = document.getElementById("fPrincipal"),
    IVA= document.getElementById("IVA"),
    facturacionF = document.getElementById("fFinal"),
    pVendedor = document.getElementById("pVendedor"),
    gVendedor = document.getElementById("gVendedor"),
    pMObra = document.getElementById("pManoObra"),
    gMObra = document.getElementById("gManoObra"),
    gEmpresa = document.getElementById("gEmpresa");
    botonCrear = document.getElementById("crear");

    let aggVenta = document.getElementById("aggVenta");


//Formulario de Editar Venta

    let tituloV2 = document.getElementById("tituloVenta2"),
    descripcionV2 = document.getElementById("descripcionVenta2"),
    facturacionP2 = document.getElementById("fPrincipal2"),
    IVA2= document.getElementById("IVA2"),
    facturacionF2 = document.getElementById("fFinal2"),
    pVendedor2 = document.getElementById("pVendedor2"),
    gVendedor2 = document.getElementById("gVendedor2"),
    pMObra2 = document.getElementById("pManoObra2"),
    gMObra2 = document.getElementById("gManoObra2"),
    gEmpresa2 = document.getElementById("gEmpresa2");
    botonEditar2 = document.getElementById("editar");

  //donde se almacenan las ventas ya creadas
    let guardarVentas=[];

  //Contador de ventas
  let contadorVentas=0;  
  let recorrido=0;



 // Reporte
  let gananciaVendedorTotal = document.getElementById("gananciaTotalVendedor");
  let gananciaManoObraTotal = document.getElementById("gananciaTotalManoObra");
  let gananciaEmpresaTotal = document.getElementById("gananciaTotalEmpresa");
  let facturacionTotal = document.getElementById("facturacionTotal");
  let botonReporte=document.getElementById("reporte");
  let botonBorrarReporte=document.getElementById("borrarReporte");
  

    
   


    


//-------------------------------------------------------------Calcular los valores cuando se crea la venta-----------------------------------------------------------------------//

let quitar = function(){

  botonCrear.removeAttribute("data-dismiss");

}

let quitar2= function(){
  botonEditar2.removeAttribute("data-dismiss");
}

//Funcion para calcular el IVA
let calculoIVA = function(){

    let resultado;
    if(facturacionP.value==""){
      facturacionF.innerHTML="0";
    }else{
      resultado =parseFloat(facturacionP.value)*parseFloat(IVA.value/100);
      resultado = parseFloat(facturacionP.value) + parseFloat(resultado);
      facturacionF.innerHTML=resultado;
    }

   
}
//Funcion para calcular la ganacnia del vendedor
let calculoVendedor = function(){
  let resultado;
  resultado = parseFloat(facturacionF.textContent)*parseFloat(pVendedor.value/100);
  gVendedor.innerHTML=resultado;
}
//funcion para calcular la ganancia de la mano de obra
let calculoManoObra = function(){
  let resultado = parseFloat(facturacionF.textContent)*parseFloat(pMObra.value/100);
  gMObra.innerHTML=resultado;
}
//funcion para calcular la ganancia de la empresa
let calculoEmpresa = function(){
  let resultado;

 
    resultado= parseFloat(facturacionF.textContent)-(parseFloat(gVendedor.textContent)+parseFloat(gMObra.textContent));
    gEmpresa.innerHTML=resultado;
}

//funcion para validar con colores
let validar = function(){

  if(tituloV.value==""){
    tituloV.setAttribute("class","vacio");
  }else{
    tituloV.setAttribute("class","lleno")
  }

  if(descripcionV.value==""){
    descripcionV.setAttribute("class","vacio");
  }else{
    descripcionV.setAttribute("class","lleno")
  }

  if(facturacionP.value==""){
    facturacionP.setAttribute("class","vacio");
  }else{
    facturacionP.setAttribute("class","lleno")
  }

  if(IVA.value==""){
    IVA.setAttribute("class","vacio");
  }else{
    IVA.setAttribute("class","lleno");
  }

  if(pVendedor.value==""){
    pVendedor.setAttribute("class","vacio");
  }else{
    pVendedor.setAttribute("class","lleno");
  }

  if(pMObra.value==""){
    pMObra.setAttribute("class","vacio");
  }else{
    pMObra.setAttribute("class","lleno");
  }

 

}


//-----------------------------------------------------------Almacena los valores dentro de la base de datos(Array)----------------------------------------------------------------//



//funcion para almacenar los datos en variables
let crearVenta= function(e){
  function venta(titulo,descripcion,montoPrin,pIVA,montoTotal,porVendedor,Tvendedor,porMano,Tmano,Tempresa){
    this.titulo=titulo;
    this.descripcion=descripcion;
    this.montoPrin = montoPrin;
    this.pIVA=pIVA;
    this.montoPrin=montoPrin;
    this.montoTotal=montoTotal;
    this.porVendedor=porVendedor;
    this.Tvendedor=Tvendedor;
    this.porMano=porMano;
    this.Tmano=Tmano;
    this.Tempresa=Tempresa;
  }

  if(tituloV.value.length == 0 ||descripcionV.value.length == 0 ||facturacionP.value.length==0 || pVendedor.value.length==0 || pMObra.value.length==0 ||IVA.value.length == 0 ){
    alert("Complete todos los campos");
    e.preventDefault();
  }else if(gEmpresa.textContent<=0){

    alert("Verifique el calculo , monto invalido");
    e.preventDefault();


  }else{

    nuevaVenta = new venta(tituloV.value,descripcionV.value,facturacionP.value,IVA.value,facturacionF.textContent, pVendedor.value,gVendedor.textContent,pMObra.value,gMObra.textContent,gEmpresa.textContent);

    agregar();

    crearCaja();

    limpiar();
    

  }

}
//funcion para agregar los elementos del formulario al array y crear las cajas en pantalla
let agregar= function(){
  guardarVentas.push(nuevaVenta);

 }
//limpia los campos despues de ser llenados con exito
let limpiar = function(){
  tituloV.value = "" ; descripcionV.value = "" ; facturacionP.value = "" ; pVendedor.value= "" ; pMObra.value= "";  IVA.value = "";

  if(  tituloV.value == "" || descripcionV.value == "" || facturacionP.value == "" || pVendedor.value== "" || pMObra.value== "" ||  IVA.value == "" ){

    facturacionF.innerHTML="0";
    gVendedor.innerHTML="0";
    gMObra.innerHTML="0";
    gEmpresa.innerHTML="0";

    tituloV.setAttribute("class"," ");
    descripcionV.setAttribute("class"," ");
    facturacionP.setAttribute("class"," ");
    IVA.setAttribute("class"," ");
    pVendedor.setAttribute("class"," ");
    pMObra.setAttribute("class"," ");

    botonCrear.setAttribute("data-dismiss","modal");

  }

}


//--------------------------------------------------------Crea las cajas que se veran en pantalla con la informacon de las ventas-----------------------------------------------------------//





//Crea la caja que se pegara en pantalla cuando se crea un venta
let crearCaja = function(){
   
  //CONTENEDOR DONDE SE COLOCARAN LOS ELEMENTOS
  let cajita = document.createElement("div");
  cajita.setAttribute("class","col caja col-12 col-md-6");
  cajita.setAttribute("id",contadorVentas);

  //Imangen que se colcara al principio
  let IMG = document.createElement("img");
  IMG.setAttribute("src", "descarga.png");
  IMG.setAttribute("class","redondo");

  //Titulo
  ttuloCajita = document.createElement("h2");
  let tContenido = document.createTextNode(guardarVentas[contadorVentas].titulo);
  ttuloCajita.appendChild(tContenido);

  //Descripcion
  descripCajita = document.createElement("p");
  let dContenido = document.createTextNode("Descripcion: " + guardarVentas[contadorVentas].descripcion);
  descripCajita.appendChild(dContenido);

  // facturacion primaria y facturacin con iva
  facturaCajita = document.createElement("p");
  let fCajita = document.createTextNode("Facturacion: " + guardarVentas[contadorVentas].montoPrin + " USD") ;
  facturaCajita.appendChild(fCajita);

  factuIVAcajita = document.createElement("p");
  let IVAcajita = document.createTextNode("Facturacion + IVA: " + guardarVentas[contadorVentas].montoTotal + " USD");
  factuIVAcajita.appendChild(IVAcajita);

  //botones de editar y eliminar

  let botonEditar = document.createElement("button");
  let texBoton1 = document.createTextNode("Editar");
  botonEditar.setAttribute("class","btn btn-warning separar");
  botonEditar.setAttribute("onClick","botonEditarCaja("+contadorVentas+"),quitar2()");
  botonEditar.setAttribute( "data-toggle","modal");
  botonEditar.setAttribute("data-target","#staticBackdrop2");
  botonEditar.appendChild(texBoton1)


  botonEliminar = document.createElement("button");
  let texBoton2 = document.createTextNode("Eliminar");
  botonEliminar.appendChild(texBoton2);
  botonEliminar.setAttribute("class", "btn btn-danger separar");
  botonEliminar.setAttribute("onClick","eliminarCaja("+contadorVentas+")");
  
  
 
 //Agregar elementos a la caja
  cajita.appendChild(IMG);
  cajita.appendChild(ttuloCajita);
  cajita.appendChild(descripCajita);
  cajita.appendChild(facturaCajita);
  cajita.appendChild(factuIVAcajita);
  cajita.appendChild(botonEditar);
  cajita.appendChild(botonEliminar);



  //agrega la caja ya creada en la pantalla
  document.getElementById("row").appendChild(cajita);



  contadorVentas++;

 


}

//Funcion del boton eliminar que se encuentra en las cajitas
let eliminarCaja = function(id) {
  let element = document.getElementById(id);
  element.remove();
 // contadorVentas--;
  
//Eliminamos la venta de la base de datos tambien / los remmplazamos con un valor cero para que el array mantenga su forma

recorrido = guardarVentas.length+recorrido;
for(let i=0;i<recorrido;i++){

      if(id==i){
        guardarVentas.splice(id,1);
        guardarVentas.splice(id,0,0);
  }
  
}








}

//funcion del boton editar en las caja
let botonEditarCaja = function (id){

  idCajaEditar = id;

  tituloV2.value=guardarVentas[id].titulo;
  descripcionV2.value=guardarVentas[id].descripcion;
 

  }



  





//-----------------------------------------------------------------Crea funciones para el formualrio de editar ventas  ---------------------------------------------//

//Funcion para calcular el IVA
let calculoIVA2 = function(){

  let resultado;
  if(facturacionP2.value==""){
    facturacionF2.innerHTML="0";
  }else{
    resultado =parseFloat(facturacionP2.value)*parseFloat(IVA2.value/100);
    resultado = parseFloat(facturacionP2.value) + parseFloat(resultado);
    facturacionF2.innerHTML=resultado;
  }

 
}

//Funcion para calcular la ganacnia del vendedor
let calculoVendedor2 = function(){
let resultado;
resultado = parseFloat(facturacionF2.textContent)*parseFloat(pVendedor2.value/100);
gVendedor2.innerHTML=resultado;
}

//funcion para calcular la ganancia de la mano de obra
let calculoManoObra2 = function(){
let resultado = parseFloat(facturacionF2.textContent)*parseFloat(pMObra2.value/100);
gMObra2.innerHTML=resultado;
}

//funcion para calcular la ganancia de la empresa
let calculoEmpresa2 = function(){
let resultado;


  resultado= parseFloat(facturacionF2.textContent)-(parseFloat(gVendedor2.textContent)+parseFloat(gMObra2.textContent));
  gEmpresa2.innerHTML=resultado;
}

//funcion para validar con colores
let validar2 = function(){

  if(tituloV2.value==""){
    tituloV2.setAttribute("class","vacio");
  }else{
    tituloV2.setAttribute("class","lleno")
  }

  if(descripcionV2.value==""){
    descripcionV2.setAttribute("class","vacio");
  }else{
    descripcionV2.setAttribute("class","lleno")
  }

  if(facturacionP2.value==""){
    facturacionP2.setAttribute("class","vacio");
  }else{
    facturacionP2.setAttribute("class","lleno")
  }

  if(IVA2.value==""){
    IVA2.setAttribute("class","vacio");
  }else{
    IVA2.setAttribute("class","lleno");
  }

  if(pVendedor2.value==""){
    pVendedor2.setAttribute("class","vacio");
  }else{
    pVendedor2.setAttribute("class","lleno");
  }

  if(pMObra2.value==""){
    pMObra2.setAttribute("class","vacio");
  }else{
    pMObra2.setAttribute("class","lleno");
  }

 

}


//Boton Editar dentro del formulario de editar
let editarVenta= function(e){
  function venta(titulo,descripcion,montoPrin,pIVA,montoTotal,porVendedor,Tvendedor,porMano,Tmano,Tempresa){
    this.titulo=titulo;
    this.descripcion=descripcion;
    this.montoPrin = montoPrin;
    this.pIVA=pIVA;
    this.montoPrin=montoPrin;
    this.montoTotal=montoTotal;
    this.porVendedor=porVendedor;
    this.Tvendedor=Tvendedor;
    this.porMano=porMano;
    this.Tmano=Tmano;
    this.Tempresa=Tempresa;
  }

  if(tituloV2.value.length == 0 ||descripcionV2.value.length == 0 ||facturacionP2.value.length==0 || pVendedor2.value.length==0 || pMObra2.value.length==0 ||IVA2.value.length == 0 ){
    alert("Complete todos los campos");
    e.preventDefault();
  }else if(gEmpresa2.textContent<=0){

    alert("Verifique el calculo , monto invalido");
    e.preventDefault();

  
  
  }else{

  ventaEditada = new venta(tituloV2.value,descripcionV2.value,facturacionP2.value,IVA2.value,facturacionF2.textContent, pVendedor2.value,gVendedor2.textContent,pMObra2.value,gMObra2.textContent,gEmpresa2.textContent);

    editar(idCajaEditar);

    limpiar2();

    editarCaja(idCajaEditar);

    botonEditar2.setAttribute("data-dismiss","modal");
    
    

  }

}
//funcion para agregar los elementos del formulario al array
let editar= function(id){
  for(let i=0;i<guardarVentas.length;i++){

    if(id==i){
      guardarVentas.splice(id,1);
      guardarVentas.splice(id,0,ventaEditada);
}

}

 }
//limpia los campos despues de ser llenados con exito
let limpiar2 = function(){
  tituloV2.value = "" ; descripcionV2.value = "" ; facturacionP2.value = "" ; pVendedor2.value= "" ; pMObra2.value= "";  IVA2.value = "";

  if(  tituloV2.value == "" || descripcionV2.value == "" || facturacionP2.value == "" || pVendedor2.value== "" || pMObra2.value== "" ||  IVA2.value == "" ){

    facturacionF2.innerHTML="0";
    gVendedor2.innerHTML="0";
    gMObra2.innerHTML="0";
    gEmpresa2.innerHTML="0";

    tituloV2.setAttribute("class"," ");
    descripcionV2.setAttribute("class"," ");
    facturacionP2.setAttribute("class"," ");
    IVA2.setAttribute("class"," ");
    pVendedor2.setAttribute("class"," ");
    pMObra2.setAttribute("class"," ");


  }

}
//en la caja en pantalla actualiza los valores
let editarCaja = function(id){
  ttuloCajita.innerHTML=guardarVentas[id].titulo;
  descripCajita.innerHTML="Descripcion: " + guardarVentas[id].descripcion;
  facturaCajita.innerHTML="Facturacion: " + guardarVentas[id].montoPrin + " USD";
  factuIVAcajita.innerHTML="Facturacion + IVA: " + guardarVentas[id].montoTotal + " USD";





}



//---------------------------------------------- Resumen//REPORTE----------------------------------------------
//Boton Reporte en pantalla
let abrirReporte=function(){

  let totalV = 0,
  totalMO = 0,
  totalE = 0,
  totalF=0;


  

  for(let i=0;i<guardarVentas.length;i++){
    if(guardarVentas[i]==0){

    }else{
      totalV =parseFloat(guardarVentas[i].Tvendedor) + parseFloat(totalV) ;

      totalMO = parseFloat(guardarVentas[i].Tmano)  + parseFloat(totalMO) ;

      totalE =parseFloat(guardarVentas[i].Tempresa ) + parseFloat(totalE) ;

      totalF = parseFloat(guardarVentas[i].montoTotal)  + parseFloat(totalF) ;
    }
  }



  gananciaVendedorTotal.innerHTML=totalV + " USD";
  gananciaManoObraTotal.innerHTML=totalMO + " USD";
  gananciaEmpresaTotal.innerText=totalE + " USD";
  facturacionTotal.innerHTML=totalF + " USD"; 


}


//boton Borrar reporte
let borrarReporte = function(){

  for(let i = 0 ; i<=guardarVentas.length; i++){
    guardarVentas.pop();
    contadorVentas--;
    elementoCaja = document.getElementById(i);
    if(elementoCaja == null){

    }else{
      elementoCaja.remove();
    }
    
  }

  gananciaVendedorTotal.innerHTML="0 USD";
  gananciaManoObraTotal.innerHTML="0 USD";
  gananciaEmpresaTotal.innerText="0 USD";
  facturacionTotal.innerHTML="0 USD"; 


}









































//------------------------------------------------------ Eventos del Formulario---------------------------------------------------------------------------

//aggVenta
aggVenta.addEventListener("click",quitar);

// Crear Venta
facturacionP.addEventListener("keyup",calculoIVA);
IVA.addEventListener("keyup",calculoIVA); 
pVendedor.addEventListener("keyup",calculoVendedor);
pMObra.addEventListener("keyup",calculoManoObra);
facturacionP.addEventListener("keyup",calculoEmpresa);
IVA.addEventListener("keyup",calculoEmpresa); 
pVendedor.addEventListener("keyup",calculoEmpresa);
pMObra.addEventListener("keyup",calculoEmpresa);
//validar crear venta
tituloV.addEventListener("keyup",validar);
descripcionV.addEventListener("keyup",validar);
facturacionP.addEventListener("keyup",validar);
IVA.addEventListener("keyup",validar); 
pVendedor.addEventListener("keyup",validar);
pMObra.addEventListener("keyup",validar);


botonCrear.addEventListener("click",crearVenta);




//Editar Venta
facturacionP2.addEventListener("keyup",calculoIVA2);
IVA2.addEventListener("keyup",calculoIVA2); 
pVendedor2.addEventListener("keyup",calculoVendedor2);
pMObra2.addEventListener("keyup",calculoManoObra2);
facturacionP2.addEventListener("keyup",calculoEmpresa2);
IVA2.addEventListener("keyup",calculoEmpresa2); 
pVendedor2.addEventListener("keyup",calculoEmpresa2);
pMObra2.addEventListener("keyup",calculoEmpresa2);
botonEditar2.addEventListener("click",editarVenta);

//validar editar venta
tituloV2.addEventListener("keyup",validar2);
descripcionV2.addEventListener("keyup",validar2);
facturacionP2.addEventListener("keyup",validar2);
IVA2.addEventListener("keyup",validar2); 
pVendedor2.addEventListener("keyup",validar2);
pMObra2.addEventListener("keyup",validar2);



//Boton Reporte
botonReporte.addEventListener("click",abrirReporte);
botonBorrarReporte.addEventListener("click",borrarReporte);




